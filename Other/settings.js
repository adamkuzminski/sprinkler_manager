var mainJS = require("./main.js");
var consts = require("./consts.js");

function Settings() {
	this._paused = 0;
	this._active_profile_id = 0;
}

Settings.prototype.getActiveProfileId = function() {
	if (this._active_profile_id == null)
		return 0;
	else
		return this._active_profile_id;
};

Settings.prototype.setActiveProfileId = function(id) {
	if (this._active_profile_id != id) {
		this._active_profile_id = id;
		mainJS.events.emit(consts.ACTIVE_PROFILE_ID_CHANGED_EVENT);
	}
};

Settings.prototype.getPaused = function() {
	return this._paused;
};

Settings.prototype.setPaused = function(paused) {
	if (this._paused != paused) {
		this._paused = paused;
		mainJS.events.emit(consts.SYSTEM_PAUSE_CHANGED_EVENT);
	}
};

Settings.prototype.isPaused = function() {
	return (this._paused == 1);
};

Settings.prototype.fromDB = function() {
	var that = this;
	mainJS.db.all("SELECT * FROM settings", 
		function(err, rows) {
			if (err) {
				mainJS.events.emit(consts.ERROR_EVENT, err);
			}
			else if (rows.length > 0) {
				that.setPaused(rows[0].paused);
				that.setActiveProfileId(rows[0].active_profile_id);
			}
		}
	);
};

Settings.prototype.toDB = function() {
	mainJS.db.run("UPDATE settings SET active_profile_id=?, paused=?",
		this._active_profile_id,
		this._paused,
		function (err) {
			if (err) {
				mainJS.events.emit(consts.ERROR_EVENT, err);
				return false;
			}
			else {
				return true;
			}
		}
	);
};


module.exports = Settings;
