CREATE TABLE `settings` (
	`active_profile_id`	INTEGER,
	`paused`	INTEGER
);
CREATE TABLE `profile_item` (
	`profile_id`	INTEGER NOT NULL,
	`start_time`	INTEGER NOT NULL,
	`stop_time`		INTEGER NOT NULL,
	`section_id`	INTEGER NOT NULL
);
CREATE TABLE `profile` (
	`name`	TEXT NOT NULL,
	`day0`	INTEGER,
	`day1`	INTEGER,
	`day2`	INTEGER,
	`day3`	INTEGER,
	`day4`	INTEGER,
	`day5`	INTEGER,
	`day6`	INTEGER
);
CREATE TABLE `log` (
	`date`	INTEGER NOT NULL,
	`type`	TEXT,
	`message`	TEXT NOT NULL
);
CREATE TABLE `custom_item` (
	`start_date`	INTEGER NOT NULL,
	`stop_date`	INTEGER NOT NULL,
	`section_id`	INTEGER NOT NULL
);
CREATE TABLE `section` (
	`name` TEXT NOT NULL,
	`type` INTEGER,
	`pin` INTEGER
);
CREATE TABLE `section_event` (
	`date` INTEGER NOT NULL,
	`type` INTEGER,
	`section_id` INTEGER NOT NULL,
	`profile_id` INTEGER
);


CREATE INDEX `profile_idx` ON `profile_item` (`profile_id` );
CREATE INDEX `section_idx` ON `profile_item` (`section_id` );
CREATE INDEX `section_idx` ON `custom_item` (`section_id` );
CREATE INDEX `profile_idx` ON `section_event` (`profile_id` );
CREATE INDEX `section_idx` ON `section_event` (`section_id` );

INSERT INTO section (name, type, pin) VALUES ('Sekcja 1 - taras', 1, 7);
INSERT INTO section (name, type, pin) VALUES ('Sekcja 2', 1, 11);
INSERT INTO section (name, type, pin) VALUES ('Sekcja 3', 1, 12);
INSERT INTO section (name, type, pin) VALUES ('Sekcja 4 - ogrodzenie', 1, 13);
INSERT INTO section (name, type, pin) VALUES ('Linia kroplująca', 0, 15);

INSERT INTO settings (active_profile_id, paused) VALUES (null, 0);