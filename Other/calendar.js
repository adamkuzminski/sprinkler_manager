var fs = require('fs');
var readline = require('readline');
var google = require('googleapis');
var googleAuth = require('google-auth-library');

var mainJS = require("./main.js");
var consts = require("./consts.js");

// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/calendar-nodejs-quickstart.json
var SCOPES = ['https://www.googleapis.com/auth/calendar'];
var TOKEN_DIR = __dirname + '/.credentials/';
var TOKEN_PATH = TOKEN_DIR + 'calendar-nodejs-quickstart.json';
var SPRINKLER_CALENDAR_ID = 'eu9jge35vp5useor6kn1jjmg6k@group.calendar.google.com';

var oauth2Client;

module.exports = {

// Load client secrets from a local file.
 init:function() {
fs.readFile('client_secret.json', function processClientSecrets(err, content) {
  if (err) {
    console.log('Error loading client secret file: ' + err);
    return;
  }
  // Authorize a client with the loaded credentials, then call the
  // Google Calendar API.
  authorize(JSON.parse(content));
});
},

/**
 * Lists the next 10 events on the user's primary calendar.
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
 listEvents:function(auth) {
  var calendar = google.calendar('v3');
  /**
  calendar.calendarList.list({
	auth: auth
  }, function(err, response) {
  console.log(response);
  });
  */
  calendar.events.list({
    auth: auth,
    calendarId: SPRINKLER_CALENDAR_ID,
    timeMin: (new Date()).toISOString(),
    maxResults: 10,
    singleEvents: true,
    orderBy: 'startTime'
  }, function(err, response) {
    if (err) {
		handleError(err);
	  console.log('The API returned an error: ' + err);
      return;
    }
    var events = response.items;
    if (events.length == 0) {
      console.log('No upcoming events found.');
    } else {
      console.log('Upcoming 10 events:');
      for (var i = 0; i < events.length; i++) {
        var event = events[i];
		console.log(event);
        var start = event.start.dateTime || event.start.date;
        console.log('%s - %s', start, event.summary);
		calendar.events.quickAdd({
			auth: auth,
			calendarId: SPRINKLER_CALENDAR_ID,
			text: event.summary + ' - Done'
		});
		/**event.description = 'Zrobione';
		calendar.events.update({
			auth: auth,
			calendarId: SPRINKLER_CALENDAR_ID,
			eventId: event.id,
			body: event
		}, function(err, response) {
			console.log(err);
			console.log(response);
		});*/
      }
    }
  });
}

}

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 *
 * @param {Object} credentials The authorization client credentials.
 */
function authorize(credentials) {
  var clientSecret = credentials.installed.client_secret;
  var clientId = credentials.installed.client_id;
  var redirectUrl = credentials.installed.redirect_uris[0];
  var auth = new googleAuth();
  oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);

  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, function(err, token) {
  console.log(TOKEN_PATH);
    if (err) {
      getNewToken();
    } else {
      oauth2Client.credentials = JSON.parse(token);
	  mainJS.events.emit(consts.CALENDAR_READY_EVENT);
    }
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 */
function getNewToken() {
  var authUrl = oauth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES
  });
  console.log('Authorize this app by visiting this url: ', authUrl);
  var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });
  rl.question('Enter the code from that page here: ', function(code) {
    rl.close();
    oauth2Client.getToken(code, function(err, token) {
      if (err) {
        console.log('Error while trying to retrieve access token', err);
        return;
      }
      oauth2Client.credentials = token;
      storeToken(token);
	  mainJS.events.emit(consts.CALENDAR_READY_EVENT);
    });
  });
}

function handleError(error) {
	if (error.code == 403)
		getNewToken();
}

/**
 * Store token to disk be used in later program executions.
 *
 * @param {Object} token The token to store to disk.
 */
function storeToken(token) {
  try {
    fs.mkdirSync(TOKEN_DIR);
  } catch (err) {
    if (err.code != 'EEXIST') {
      throw err;
    }
  }
  fs.writeFile(TOKEN_PATH, JSON.stringify(token));
  console.log('Token stored to ' + TOKEN_PATH);
}

