//================= REQUIRES ==================//
var consts = require("./consts.js");

var SQLite3 = require("sqlite3").verbose();
var file = __dirname + "/data.db";
var	fs = require("fs");
var exists = fs.existsSync(file);
var db = new SQLite3.Database(file);

var gpio = require("rpi-gpio");
gpio.setMode(gpio.MODE_RPI);

var Express = require("express");
var app = Express();
var bodyParser = require("body-parser");

var Events = require("events");
var events = new Events.EventEmitter();

var async = require("async");

exports.events = events;
exports.db = db;

var Settings = require("./settings.js");
var settings = new Settings();

var section_to_pin = [];
var updating_schedule = false;

//================== EVENTS ===================//
events.on(consts.DATABASE_READY_EVENT, function() {
	console.log("Database ready");
	
	settings.fromDB();

	//GPIO setup
	db.each("SELECT rowid, pin FROM section", 
		function(err, row) {
			if (err) {
				logE(err);
			}
			else {
				gpio.setup(row.pin, gpio.DIR_OUT, function(err) {
					section_to_pin[row.rowid] = row.pin;
					gpio.write(row.pin, 0);
				});
			}
		}
	);
	
	gpio.setup(consts.TRANS_PIN, gpio.DIR_OUT, function() {
		//Main loop
		setInterval( function() {
			if (true) {
				//console.log("Paused: " + settings.isPaused() + ", profile id: " + settings.getActiveProfileId());
				
				var date = new Date();
				var minutes = date.getHours() * 60 + date.getMinutes();
				var from = 2*60+00;
				var length = 40;
				var section1 = (minutes >= from && minutes < from + length);
				from += length;
				var section2 = (minutes >= from && minutes < from + length);
				from += length;
				var section3 = (minutes >= from && minutes < from + length);
				from += length;
				var section4 = (minutes >= from && minutes < from + length);
				var sprinklers_on = section1 || section2 || section3 || section4;
				
				gpio.write(15, section1? 1 : 0, function(err) {
					if (err)
						console.log(err);
				});
				gpio.write(13, section2? 1 : 0, function(err) {
					if (err) 
						console.log(err);
				});
				gpio.write(12, section3? 1 : 0, function(err) {
					if (err) 
						console.log(err);
				});
				gpio.write(11, section4? 1 : 0, function(err) {
					if (err) 
						console.log(err);
				});
				gpio.write(consts.TRANS_PIN, sprinklers_on? 1 : 0, function(err) {
					if (err) 
						console.log(err);
				});
				
				console.log(date + ": " + (section1? "1" : "0") + (section2? "1" : "0") + (section3? "1" : "0") + (section4? "1" : "0") + ", TRANS = " + sprinklers_on);
			}
		}, 10000);
	});
	
	//Update schedule
	setInterval( function() {
		//updateSchedule();
	}, 3600000*4); //4h
});

events.on(consts.SYSTEM_PAUSE_CHANGED_EVENT, function() {
	console.log("System pause changed to " + settings.isPaused());
});

events.on(consts.ACTIVE_PROFILE_ID_CHANGED_EVENT, function() {
	console.log("Active profile id changed to " + settings.getActiveProfileId());
	updateSchedule();
});

events.on(consts.ERROR_EVENT, function(err) {
	logE(err);
});

//================= DATABASE ==================//
db.serialize(function() {
	if (exists) {
		events.emit(consts.DATABASE_READY_EVENT);
	}
	else {
		// Prepare empty database
		console.log("Creating DB file.");
		fs.openSync(file, "w");
		fs.readFile(__dirname + "/def.sql", 'utf8', function (err, data) {
			if (err) {
				return console.log(err);
			}
			var sqllines = data.split(';');
			
			(function next() {
				if (sqllines.length) {
					db.run(sqllines.shift(), next);
				}
			}());
			
			events.emit(consts.DATABASE_READY_EVENT);
		});
	}
});

//================= WEB SERVER ==================//
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));
var server = app.listen(8081, function () {
	var host = server.address().address
	var port = server.address().port
	console.log("Listening at http://%s:%s", host, port)
})

//------------------ Profiles -------------------//
//Delete profile
app.delete('/profile/:id', function (req, res) {
	db.run("DELETE FROM profile WHERE rowid=?", 
		req.params.id,
		function(err) {
			if (err) {
				logE(err);
				res.send(err);
				res.status(500);
			}
			else {
				if (settings.getActiveProfileId() == req.params.id) {
					settings.setActiveProfileId(null);
					settings.toDB();
				}
			
				db.run("DELETE FROM profile_item WHERE profile_id=?",
					req.params.id,
					function(err) {
						if (err) {
							logE(err);
							res.send(err);
							res.status(500);
						} 
						else {
							res.end();
						}
					}
				);
			}
		}
	);
})

//Get all profiles
app.get('/profile', function (req, res) {
	db.all("SELECT rowid, * FROM profile ORDER BY name", 
		function(err, rows) {
			if (err) {
				logE(err);
				res.send(err);
				res.status(500);
			}
			else {
				res.send(JSON.stringify(rows));
			}
		}
	);
})

//Get profile
app.get('/profile/:id', function (req, res) {	
	db.get("SELECT * FROM profile WHERE rowid=?", 
		req.params.id,
		function(err, row) {
			if (err) {
				logE(err);
				res.send(err);
				res.status(500);
			}
			else {
				res.send(JSON.stringify(row));
			}
		}
	);
})

//Add new profile
app.post('/profile', function (req, res) {
	db.run("INSERT INTO profile (name, day0, day1, day2, day3, day4, day5, day6) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
		req.body.name,
		req.body.day0,
		req.body.day1,
		req.body.day2,
		req.body.day3,
		req.body.day4,
		req.body.day5,
		req.body.day6,
		function (err) {
			if (err) {
				logE(err);
				res.send(err);
				res.status(500);
			}
			else {
				res.send('RowId=' + this.lastID);
			}
		}
	);
})

//Modify profile
app.post('/profile/:id', function (req, res) {
	db.run("UPDATE profile SET name=?, day0=?, day1=?, day2=?, day3=?, day4=?, day5=?, day6=? WHERE rowid=?",
		req.body.name,
		req.body.day0,
		req.body.day1,
		req.body.day2,
		req.body.day3,
		req.body.day4,
		req.body.day5,
		req.body.day6,
		req.params.id,
		function (err) {
			if (err) {
				logE(err);
				res.send(err);
				res.status(500);
			}
			else {
				if (settings.getActiveProfileId() == req.params.id)
					updateSchedule();
					
				res.end();
			}
		}
	);
})

//------------------ Profile items -------------------//
//Delete profile item
app.delete('/profileitem/:profile_id/:id', function (req, res) {
	db.run("DELETE FROM profile_item WHERE rowid=?", 
		req.params.id,
		function(err) {
			if (err) {
				logE(err);
				res.send(err);
				res.status(500);
			}
			else {
				if (settings.getActiveProfileId() == req.params.profile_id)
					updateSchedule();
			
				res.end();
			}
		}
	);
})

//Get all profile items
app.get('/profileitem/:profile_id', function (req, res) {
	db.all("SELECT rowid, * FROM profile_item WHERE profile_id=? ORDER BY start_time", 
		req.params.profile_id,
		function(err, rows) {
			if (err) {
				logE(err);
				res.send(err);
				res.status(500);
			}
			else {
				res.send(JSON.stringify(rows));
			}
		}
	);
})

//Get profile item
app.get('/profileitem/:profile_id/:id', function (req, res) {
	db.get("SELECT * FROM profile_item WHERE rowid=?", 
		req.params.id,
		function(err, row) {
			if (err) {
				logE(err);
				res.send(err);
				res.status(500);
			}
			else {
				res.send(JSON.stringify(row));
			}
		}
	);
})

//Add new profile item
app.post('/profileitem', function (req, res) {
	db.run("INSERT INTO profile_item (profile_id, start_time, stop_time, section_id) VALUES (?, ?, ?, ?)",
		req.body.profile_id,
		req.body.start_time,
		req.body.stop_time,
		req.body.section_id,
		function (err) {
			if (err) {
				logE(err);
				res.send(err);
				res.status(500);
			}
			else {
				if (settings.getActiveProfileId() == req.body.profile_id)
					updateSchedule();
			
				res.send('RowId=' + this.lastID);
			}
		}
	);
})

//Update profile item
app.post('/profileitem/:id', function (req, res) {
	db.run("UPDATE profile_item SET profile_id=?, start_time=?, stop_time=?, section_id=? WHERE rowid=?",
		req.body.profile_id,
		req.body.start_time,
		req.body.stop_time,
		req.body.section_id,
		req.params.id,
		function (err) {
			if (err) {
				logE(err);
				res.send(err);
				res.status(500);
			}
			else {
				if (settings.getActiveProfileId() == req.body.profile_id)
					updateSchedule();
			
				res.end();
			}
		}
	);
})


//------------------ Custom items -------------------//
//Delete custom item
app.delete('/customitem/:id', function (req, res) {
	db.run("DELETE FROM custom_item WHERE rowid=?", 
		req.params.id,
		function(err) {
			if (err) {
				logE(err);
				res.send(err);
				res.status(500);
			}
			else {
				updateSchedule();
		
				res.end();
			}
		}
	);
})

//Get all custom items from date
app.get('/customitem/fromdate/:from_date', function (req, res) {
	db.all("SELECT rowid, * FROM custom_item WHERE start_date>=? ORDER BY start_date", 
		req.params.profile_id,
		function(err, rows) {
			if (err) {
				logE(err);
				res.send(err);
				res.status(500);
			}
			else {
				res.send(JSON.stringify(rows));
			}
		}
	);
})

//Get custom item
app.get('/customitem/:id', function (req, res) {
	db.get("SELECT * FROM custom_item WHERE rowid=?", 
		req.params.id,
		function(err, row) {
			if (err) {
				logE(err);
				res.send(err);
				res.status(500);
			}
			else {
				res.send(JSON.stringify(row));
			}
		}
	);
})

//Add new custom item
app.post('/customitem', function (req, res) {
	db.run("INSERT INTO custom_item (start_date, stop_date, section_id) VALUES (?, ?, ?)",
		req.body.start_date,
		req.body.stop_date,
		req.body.section_id,
		function (err) {
			if (err) {
				logE(err);
				res.send(err);
				res.status(500);
			}
			else {
				updateSchedule();
			
				res.send('RowId=' + this.lastID);
			}
		}
	);
})

//Update custom item
app.post('/customitem/:id', function (req, res) {
	db.run("UPDATE custom_item SET start_date=?, stop_date=?, section_id=? WHERE rowid=?",
		req.body.start_date,
		req.body.stop_date,
		req.body.section_id,
		req.params.id,
		function (err) {
			if (err) {
				logE(err);
				res.send(err);
				res.status(500);
			}
			else {
				updateSchedule();
			
				res.end();
			}
		}
	);
})

//------------------ Sections -------------------//
//Delete section
app.delete('/section/:id', function (req, res) {
	db.run("DELETE FROM section WHERE rowid=?", 
		req.params.id,
		function(err) {
			if (err) {
				logE(err);
				res.send(err);
				res.status(500);
			}
			else {
				res.end();
			}
		}
	);
})

//Get all sections
app.get('/section', function (req, res) {
	db.all("SELECT rowid, * FROM section ORDER BY name", 
		function(err, rows) {
			if (err) {
				logE(err);
				res.send(err);
				res.status(500);
			}
			else {
				res.send(JSON.stringify(rows));
			}
		}
	);
})

//Get section
app.get('/section/:id', function (req, res) {
	db.get("SELECT * FROM section WHERE rowid=?", 
		req.params.id,
		function(err, row) {
			if (err) {
				logE(err);
				res.send(err);
				res.status(500);
			}
			else {
				res.send(JSON.stringify(row));
			}
		}
	);
})

//Add new section
app.post('/section', function (req, res) {
	db.run("INSERT INTO section (name, type, pin) VALUES (?, ?, ?)",
		req.body.name,
		req.body.type,
		req.body.pin,
		function (err) {
			if (err) {
				logE(err);
				res.send(err);
				res.status(500);
			}
			else {
				res.send('RowId=' + this.lastID);
			}
		}
	);
})

//Update section
app.post('/section/:id', function (req, res) {
	db.run("UPDATE section SET name=?, type=?, pin=? WHERE rowid=?",
		req.body.name,
		req.body.type,
		req.body.pin,
		req.params.id,
		function (err) {
			if (err) {
				logE(err);
				res.send(err);
				res.status(500);
			}
			else {
				res.end();
			}
		}
	);
})

//------------------ Settings -------------------//
//Get settings
app.get('/settings', function (req, res) {
	db.all("SELECT * FROM settings", 
		function(err, rows) {
			if (err) {
				logE(err);
				res.send(err);
				res.status(500);
			}
			else {
				res.send(JSON.stringify(rows[0]));
			}
		}
	);
})

//Set settings
app.post('/settings', function (req, res) {
	if ((settings.getActiveProfileId() != req.body.active_profile_id) || (settings.getPaused() != req.body.paused)) {
		settings.setActiveProfileId(req.body.active_profile_id);
		settings.setPaused(req.body.paused);
		if (settings.toDB()) 
			res.end();
		else
			res.status(500);
	}
})

//================== Other functions ==================//
function logE(error) {
	console.log(error);
	db.run("INSERT INTO log (date, type, message) VALUES (?, ?, ?)", 
		new Date().toISOString(),
		"ERROR",
		error
	)
}

function updateSchedule() {
	console.log("Update schedule");
	updating_schedule = true;
	
	var date = new Date();
	
	var profile_id = settings.getActiveProfileId();
	//Profile profile = null;
	
	async.series([
		function(callback){
			//Get profile
			db.get("SELECT * FROM profile WHERE rowid=?", 
				profile_id,
				function(err, row) {
					if (err) {
						callback(err, "Get profile");
					}
					else {
						var day = date.getDay();
						if (row["day" + day] == 1) {
							db.all("SELECT rowid, * FROM profile_item WHERE profile_id=? ORDER BY start_time", 
								profile_id,
								function(err, rows) {
									if (err) {
										callback(err, "Get profile items");
									}
									else {
										console.log(rows);
										callback(null, "Get profile and items");
									}
								}
							)
						}				
					}
				}
			);
		},
		function(callback){
			console.log("2");
			callback(null, 'two');
		}
	],
	function(err, results){
		console.log("async err: " + err + ", results: " + results);
		// results is now equal to ['one', 'two']
	});

	updating_schedule = false;
}

//db.close();