module.exports = Object.freeze({
    ACTIVE_PROFILE_ID_CHANGED_EVENT: "active_profile_id_changed",
	CALENDAR_READY_EVENT: "calendar_ready",
	DATABASE_READY_EVENT: "database_ready",
	ERROR_EVENT: "error",
    SYSTEM_PAUSE_CHANGED_EVENT: "system_pause_changed_event",
	TRANS_PIN: 22
});
