//================= INIT ==================//
var gpio = require("rpi-gpio");
gpio.setMode(gpio.MODE_RPI);

var Events = require("events");
var events = new Events.EventEmitter();

var fs = require('fs');
var readline = require('readline');
var google = require('googleapis');
var googleAuth = require('google-auth-library');

// https://elinux.org/RPi_Low-level_peripherals
var section_pins = [15, 13, 12, 11, 7, 16, 18];
var section_status = [];
for (i=0; i<section_pins.length; i++)
	section_status[i] = 2;
var pins_set = 0;

var oauth2Client;
var calendar_ready = false;
var silientMode = false;
var start_date = null;
var end_date = null;
var section_sequence = null;

const CALENDAR_READY_EVENT = "calendar_ready";
const PIN_READY_EVENT = "pin_ready";

const TRANS_PIN = 22;

const SCOPES = ['https://www.googleapis.com/auth/calendar.readonly'];
const TOKEN_DIR =  __dirname + '/.credentials/';
const TOKEN_PATH = TOKEN_DIR + 'calendar-nodejs-quickstart.json';
const SPRINKLER_CALENDAR_ID = 'eu9jge35vp5useor6kn1jjmg6k@group.calendar.google.com';

logI("Sprinklers manager v.1.4 by DRAK");
logI("In case of problems with PINs start program with root or sudo permission");

section_pins.forEach(function (item, index, array) {
	gpio.setup(item, gpio.DIR_OUT, function() {
		events.emit(PIN_READY_EVENT);
	});
});

gpio.setup(TRANS_PIN, gpio.DIR_OUT, function() {
	events.emit(PIN_READY_EVENT);
});

// Google Calendar API - Load client secrets from a local file.
fs.readFile(__dirname + '/client_secret.json', function processClientSecrets(err, content) {
	if (err) {
		logE("Error loading client secret file: " + err);
		return;
	}
	// Authorize a client with the loaded credentials, then call the Google Calendar API.
	authorize(JSON.parse(content));
});

//================== EVENTS ===================//
events.on(PIN_READY_EVENT, function() {
	pins_set++;
	if (pins_set == section_pins.length + 1) {
		logI("PINs ready");
		//Main loop
		setInterval( function() {
			if (calendar_ready) {
				checkCalendar(oauth2Client);
			}
			else {
				logI("Calendar not ready");
			}		
		}, 10000);
		
		setInterval( function() {
			var status = [];
			var i;
			for (i=0; i<section_pins.length; i++)
				status[i] = 0;
			
			var trans_status = 0;
			var now = new Date();
			if ((start_date != null) &&(now >= start_date) && (now <= end_date)) {
				var duration = end_date.getTime() - start_date.getTime();
				if (duration > 0) {
					var section_duration = duration / section_sequence.length;
					var section_index = Math.floor((now.getTime() - start_date.getTime())/section_duration);
					status[section_sequence[section_index] - 1] = 1;
					trans_status = 1;
				}
			}
			
			var modified = false;
			for (i=0; i<status.length; i++) {
				if (section_status[i] != status[i]) {
					(function(i) {
						gpio.write(section_pins[i], status[i], function(err) {
							if (err) {
								logE(err);
							}
							else {
								section_status[i] = status[i];
								logI("Section " + i + " status modified to " + status[i]);
							}
						});
					})(i);
					
					modified = true;
				}
			}
			if (modified) {
				gpio.write(TRANS_PIN, trans_status, function(err) {
					if (err) {
						logE(err);
					}
					else {
						logI("Trans status modified to " + trans_status);
					}
				});
			}
			
		}, 2000);
	};
});

events.on(CALENDAR_READY_EVENT, function() {
	checkCalendar(oauth2Client);
	calendar_ready = true;
});

//================== Other functions ==================//
function logE(error) {
	if (!silientMode)
		console.log("[ERROR] " + (new Date()).toISOString() + ": " + error);
}

function logI(info) {
	if (!silientMode)
		console.log("[INFO] " + (new Date()).toISOString() + " : " + info);
}

//================== CALENDAR =====================//
//Create an OAuth2 client with the given credentials, and then execute the given callback function.
function authorize(credentials) {
	var clientSecret = credentials.installed.client_secret;
	var clientId = credentials.installed.client_id;
	var redirectUrl = credentials.installed.redirect_uris[0];
	var auth = new googleAuth();
	oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);

	// Check if we have previously stored a token.
	fs.readFile(TOKEN_PATH, function(err, token) {
		if (err) {
			getNewToken(oauth2Client);
		} else {
			oauth2Client.credentials = JSON.parse(token);
			events.emit(CALENDAR_READY_EVENT);
		}
	});
}

//Get and store new token after prompting for user authorization, and then execute the given callback with the authorized OAuth2 client.
function getNewToken(oauth2Client) {
	var authUrl = oauth2Client.generateAuthUrl({
		access_type: 'offline',
		scope: SCOPES
	});
	logI("Authorize this app by visiting this url: " + authUrl);
	silientMode = true;
	var rl = readline.createInterface({
		input: process.stdin,
		output: process.stdout
	});
	rl.question('Enter the code from that page here: ', function(code) {
		rl.close();
		oauth2Client.getToken(code, function(err, token) {
			silientMode = false;
			if (err) {				
				logE("Error while trying to retrieve access token " + err);
				return;
			}
			oauth2Client.credentials = token;
			storeToken(token);
			events.emit(CALENDAR_READY_EVENT);
		});
	});
}

//Store token to disk be used in later program executions.
function storeToken(token) {
	try {
		fs.mkdirSync(TOKEN_DIR);
	} catch (err) {
		if (err.code != 'EEXIST') {
			throw err;
		}
	}
	fs.writeFile(TOKEN_PATH, JSON.stringify(token));
	logI("Token stored to " + TOKEN_PATH);
}

function checkCalendar(auth) {
	var calendar = google.calendar('v3');
	calendar.events.list({
		auth: auth,
		calendarId: SPRINKLER_CALENDAR_ID,
		timeMin: (new Date()).toISOString(),
		maxResults: 1,
		singleEvents: true,
		orderBy: 'startTime'
	}, function(err, response) {
		if (err) {
			logE("The API returned an error: " + err);
			return;
		}
		var events = response.items;
		if (events.length == 0) {
			logI("No upcoming events found");
			start_date = null;
			end_date = null;
		} else {
			var event = events[0];
			start_date = new Date(event.start.dateTime || event.start.date);
			end_date = new Date(event.end.dateTime || event.end.date);				
			logI("Event found: " + start_date);
			
			var data = event.description;
			if ((data == null) || !data.trim())
				data = "1234";
			section_sequence = data.match(/[1-7]/g);
			logI(section_sequence);
		}
	});
}
